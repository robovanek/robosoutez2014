API robota
----------

Mechanics.java - fyzické ovládání robota
  Proměnné:
    SPEED - "aktuální" rychlost
    usPos - pozice ultrazvukového senzoru
    
  Konstruktor:
    Mechanics(diameter, track, wheelbase) - inicializuje s poloměrem kola, šířkou nápravy a vzdáleností mezi koly v řadě
    
  Funkce:
    turn(toRight, degrees) - otočí se v daném směru o tolik stupňů, funguje zde 3-zónová korekce
    turnP(toRight, degrees) - otočí se v daném směro o tolik stupňů, funguje zde proporcionální korekce
    
    travel(backward) - začne neomezenou jízdu
    travel(backward, distance) - ujede danou vzdálenost současnou rychlostí
    travel(backward, distance, nonblock) - ujede danou vzdálenost současnou rychlostí, případně asynchronně
    travel(backward, distance, speed) - ujede danou vzdálenost v dané rychlosti v cm/s
    travel(backward, distance, speed, nonblock) - ujede danou vzdálenost v dané rychlosti v cm/s, případně asynchronně
    
    rotateUSto(orientation) - otočí ultrazvukový senzor daným směrem

  Podtřídy:
 SF Direction - otočení senzoru
 SF     Left,Straight,Right
 SF Rotator - asynchronní otáčeč motoru - proto se může otáčet více motorů najednou:
 S      Rotator rotate(motor,degrees) - otočí async motor o ... stupňů
        




Testmain.java - testy




Map.java - mapa herní plochy

  Proměnné & konstanty:
 SF sizeX=11, sizeY=8 - velikost mapy
 SF center=5,4 - střed
    curOrient=North - současné natočení
    curPos=center - současná pozice
 SF maxWalls - maximum zdí
    curWalls - současný počet objevených zdí
    
  Konstruktor:
    Map()

  Funkce:
    turn(toRight,quarters) - otočit směrěm a počtem 90°
    travel(count,backward) - poodjet v mapě a nastavit projetá pole na LightOff
    writeDetection(freeTiles,orientation, wall) - zapíše detekované údaje do mapy a případně zapíše zeď
    inFront(orientation,tile) - počet polí před aktuální pozicí
    Path  BFSearch(posStart, posReq, walls[]) - projde mapu a vrátí nejbližší cestu k posReq
    Path  BFSearch(posStart, tileReq, walls[]) - projde mapy a vrátí nejbližší cestu k tileReq
    Pos[]   findAll(tile) - najde všechny pole, kde je tile
    {Pos,Path}  findAllPath(tile, walls[]) - najde cestu pro každý dostupný tile
    {Pos,Path}  findAllPath(startPos, tile, walls[]) - najde cestu pro každý dostupný tile
  Podtřídy:
    Orient:
        North,East,South,West
        Edit(actual,edit) - otočí se o tolik doprava(+) nebo doleva(-)
        usEdit(actual,sensor) - převede otočení senzoru do absolutního otočení
    Tile:
        LightOn,LightOff,Unknown,Wall



Pos.java - souřadnicová abstrakce
  Proměnné:
    x - vodorovná souřadnice X
    y - svislá souřadnice Y
  Konstruktor:
    Pos(x,y) - inicializuje s X,Y
  Funkce:
    Pos modify(orig,orient,count) - posune danou pozici v daném směru o daný počet polí
  S Pos[] adjacent(pos) - vrátí 4 okolní pozice
  S int directionOf(orig, next[,count]) - vrátí směr jízdy potřebný k dorazení z orig do next
    equals;hashCode;toString - podpůrné metody
	
	
Path.java - abstrakce cesty
  Proměnné:
    path - samotná cesta implementovaná jako fronta pozic
    
  Konstruktor:
    Path(path) - inicializuje a předá path dovnitř
    
  Funkce:
    length() - vrátí délku dráhy
    follow(Main program) - TODO; projíždí cestu a přitom zaznamenává měření

Common.java - pomocné funkce nepatřící nikam
 SF  boolean aContainsO(o,a[]) - zda-li obsahuje pole a element o

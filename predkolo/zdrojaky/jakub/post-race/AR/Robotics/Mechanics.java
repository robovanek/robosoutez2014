package AR.Robotics;

import AR.Const;
import lejos.nxt.*;
import lejos.nxt.addon.*;

/**
 * Rozhraní pro fyzický pohyb robota
 * TODO kompletně překopat, viz FIXME.txt
 * @author Jakub Vaněk
 * @author Filip Smola
 */
public class Mechanics {

	// --- PROMĚNNÉ A KONSTANTY ---
	/**
	 * Rychlost motorů ve stupních za sekundu
	 */
	public int SPEED = 360;
	/**
	 * Zatáčecí konstanta (poměr mezi otočením kol a otočením robota)
	 */
	public double turn_constant;
	/**
	 * Přímočará konstanta (obvod kol)
	 */
	public float straigth_constant;
    /**
	 * Záznamník gyroskopu
	 */
	private GyroDirectionFinder heading;
	/**
	 * Levý motor
	 */
	private NXTRegulatedMotor left;
	/**
	 * Pravý motor
	 */
	private NXTRegulatedMotor right;
	/**
	 * Maximální odchylka při zatáčení
	 */
	private static final int maxError = 5;
	/**
	 * Proporcionální konstanta korekce zatáčení
	 */
	private static final float Kp = 1f;
	
	
	// --- KONSTRUKTOR ---
	/**
	 * Inicializuje zařízení a konstanty
	 * TODO konstanty by se mohly cachovat na interní disk, přestože to není to nejpomalejší
	 */
	public Mechanics() {
		float wheelDiameter = Const.WHEELDIAMETER, trackWidth = Const.TRACKWIDTH, wheelBase = Const.WHEELBASE;
		System.out.print("Mechanics init..");

		this.left = new NXTRegulatedMotor(Const.leftMotor);
		this.right = new NXTRegulatedMotor(Const.rightMotor);
		turn_constant = (Math.sqrt(trackWidth * trackWidth + wheelBase
				* wheelBase)          // odmocnina počítá průměr kruhu opsaného obdélníku kol
				/ wheelDiameter)*1.2; // to se vydělí průměrem kola; 1.2 je doladění
		straigth_constant = (float) (wheelDiameter * Math.PI); // obvod kola
		GyroSensor gyroscope = new GyroSensor(Const.gyroPort); // definice gyroskopu
		gyroscope.recalibrateOffset(); // kalibrace trvá dlouho... chrr... celých 5 vteřin
		heading = new GyroDirectionFinder(gyroscope, false); // definice záznamníku
		putSpeed(SPEED); // nastavení rychlosti motorů
		System.out.print("Hotovo :)");
	}

	// --- PODPŮRNÉ FUNKCE ---
	/**
	 * Získá otočení kol pro otočení robota o daný úhel
	 * 
	 * @param degree
	 *            Požadovaný úhel
	 * @return Úhel pro motory
	 */
	private int turn_degree(int degree) {
		return (int) (turn_constant * degree);  // matematická kouzla, už si nevzpomínám...
	}                                           // ale bude to násobení rotace robota poměrem otočení kol a otočení robota

	/**
	 * Získá otočení kol pro ujetí dané vzdálenosti
	 * 
	 * @param distance
	 *            Vzdálenost v cm
	 * @return Úhel pro motory
	 */
	private int straight_degree(float distance) {
		return (int) (distance / straigth_constant * 360);  // vzdálenost se vydělí obvodem kola, to dá počet otáček kola
	}

	/**
	 * Nastaví rychlost levého a pravého motoru
	 * @param speed Úhlová rychlost motorů
	 */
	private void putSpeed(float speed)
	{
		left.setSpeed(speed);
		right.setSpeed(speed);
	}

	// --- VEŘEJNÉ API ---
	/**
	 * Otočí robota o daný úhel a zkoriguje jej pomocí proporcionálního regulátoru
	 *
	 * @param goRight
	 *            Jestli zatáčet doprava (CW) (true) nebo doleva (CCW) (false)
	 * @param degree
	 *            O jaký úhel se otočit
	 */
	public void turn(boolean goRight, int degree){
		heading.resetCartesianZero(); // reset trackeru úhlu
		int rotation = turn_degree(degree); // zjistit o kolik stupňů otočit kola
		putSpeed(SPEED / 2); // nastavit poloviční rychlost, protože 1 strana točí jen 1/2 robota
		int negative = goRight ? 1 : -1; // přepínání mezi jízdou doleva a doprava
		// začít otáčení motorů
		AsyncMotor l = AsyncMotor.rotate(left, rotation * negative); // pravá+,levá-
		AsyncMotor r = AsyncMotor.rotate(right, -rotation * negative); // pravá-,levá+
		AsyncMotor.waitFor(l, r); // počkat na dokončení otočky
		int error; // níže: dokud je absolutní hodnota odchylky větší než únosná hodnota
		while(Math.abs(error = degree + negative * (int) heading.getDegreesCartesian())>maxError) {
			// kladné nedotočení, záporné přetočení                // pravá degree + [], levá degree - []
			int fix = turn_degree((int) (Kp * error));  // spočítat proporcionální opravu konstanta krát odchylka ROBOTA,
														// proto to celé dát výpočtu otočení KOL
			l = AsyncMotor.rotate(left, negative * fix); // začít točit
			r = AsyncMotor.rotate(right, negative * -fix);
			AsyncMotor.waitFor(l, r); // a počkat
		}
		// návrat na normální rychlost
		putSpeed(SPEED);
	}

	/**
	 * Ujede danou vzdálenost danou rychlostí
	 * 
	 * @param centimeters
	 *            Vzdálenost v cm
	 * @param backward
	 *            Jestli jet dopředu (false) nebo dozadu (true)
	 * @param speed
	 *            Rychlost v cm/s
	 */
	public void travel(boolean backward, float centimeters, int speed) {
		// zjistit o kolik otočit motory
		int rotation = straight_degree(centimeters);
		// vypočítat úhlovou rychlost motorů
		int velocity = (int) ((speed/straigth_constant)*360);
		// nastavit rychlost
		putSpeed(velocity);
		// pokud jedeme dozadu, točit dozadu
		if (backward)
			rotation *= -1;
		// začít točit motory
		AsyncMotor l = AsyncMotor.rotate(left, rotation);
		AsyncMotor r = AsyncMotor.rotate(right, rotation);
		AsyncMotor.waitFor(l, r); // počkat na dokončení
		// nastavit původní rychlost
		putSpeed(SPEED);
	}

	/**
     * Nastaví úhlovou rychlost SPEED na danou rovinnou rychlost
     * @param speed Rychlost v cm/s
     */
	public void setSPEED(int speed)
    {
        SPEED = (int) ((speed/straigth_constant)*360); // rychlost na rovině se vydělí obvodem kola (otočení kol/s)
	    putSpeed(SPEED); // nastavení rychlosti         // kolo má 360°, proto se to vše vynásobí 360°
    }


	/**
	 * Nastaví úhlové zrychlení motorů (l+r.setAcceleration)
	 * @param accel Zrychlení ve °/s
	 */
	public void setAccel(int accel)
	{
		left.setAcceleration(accel);
		right.setAcceleration(accel);
	}


	/**
	 * Spustí motory a nechá je běžet
	 * 
	 * @param backward
	 *            Jestli jet dopředu (false) nebo dozadu (true)
	 */
	public void travel(boolean backward) {
		// pokud jedeme dopředu
		if (!backward) { // točit dopředu
			right.forward();
			left.forward();
		} else { // jinak točit dozadu
			right.backward();
			left.backward();
		}
	}
}

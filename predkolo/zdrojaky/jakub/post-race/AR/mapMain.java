package AR;

import AR.Utils.MapUI;
import lejos.nxt.Sound; // zvuk
import lejos.util.Delay; // sleep

/**
 * Alternativní main() pro výběr mapy
 */
public final class mapMain {
	/**
	 * Počet map
	 */
	public static final int MAXVALUE=8;

	/**
	 * Hlavní funkce
	 * @param args nic
	 */
	public static void main(String[] args)
	{
		Sound.beep(); // pípne
		Delay.msDelay(500); // počká 0.5 s
		Sound.beep(); // pípne
		int number = MapUI.getNumber(MAXVALUE); // zobrazí menu a získá číslo
		Main m = new Main(); // vytvoří instanci řídícího programu
		m.init(); // inicializuje program (např. počítá hodnoty v AR.Robotics.Mechanics)
		m.load(number); // načte mapu, ekvivalent úplného prohledání mapy (ale beze sběru)
		m.run(); // spustí program
	}
}

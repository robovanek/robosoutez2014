/**
 * Common stuff
 */
public final class Common
{
	/**
	 * Array helper function
	 * @param o Object to search
	 * @param a Array where search
	 * @return Whether object is in array
	 */
	public static boolean aContainsO(int o, int[] a)
	{
		for(int element: a)
			if(element==o)
				return true;
		return false;
	}
    /**
     * Array helper function
     * @param o Object to search
     * @param a Array where search
     * @return Whether object is in array
     */
    public static boolean aContainsO(Object o, Object[] a)
    {
        for(Object element: a)
            if(element==o)
                return true;
        return false;
    }
    /**
     * Rounds the number
     * @param number Number to round
     * @param limit Rounding limit: If the floating part is bigger or equal, round up; else, round down. Must be 0<limit<1.
     * @return Rounded number
     */
    public static int round(float number, float limit)
    {
        float floatPart = number%1;
        if(floatPart >= limit)
            return (int) Math.ceil(number);
        else
            return (int) Math.floor(number);
    }
	private Common(){}
}

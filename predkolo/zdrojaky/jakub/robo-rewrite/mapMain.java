import lejos.nxt.Sound;
import lejos.util.Delay;

/**
 * Alternativa main()
 */
public final class mapMain {
	public static final int MAXVALUE=8;
	public static void main(String[] args)
	{
		Sound.beep();
		Delay.msDelay(500);
		Sound.beep();
		int number = MapUI.getNumber(MAXVALUE);
		Main m = new Main();
		m.init();
		m.load(number);
		m.run();
	}
}

/**
 * Fast exception
 * @author Jakub Vaněk
 */
public class FoundException extends Exception {
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
}
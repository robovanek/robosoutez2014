import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.util.Delay;

/**
 * Kills the program after pressing specified button
 */
public class Killer extends Thread {
    boolean kill = true;
    Button b;
    Mechanics m;
    Thread original;
    /**
     * Constructs Killer with specified killing button
     */
    public Killer(Button b, Mechanics m, Thread orig)
    {
        this.b = b;
        this.m = m;
        original = orig;
    }
    public void run()
    {
        while(original.isAlive()) {
            if(b.isDown()) {
                System.out.println("Pressed "+b.toString()+"; exiting...");
                original.interrupt();
                System.exit(0);
            }
            Delay.msDelay(100);
        }
    }
}

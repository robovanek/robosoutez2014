import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

/**
 * Created by kuba on 9.11.14.
 */
public class PressTest {
	public static void main(String[] args)
	{
		TouchSensor s = new TouchSensor(SensorPort.S4);
		LCD.clear();
		while(Button.ESCAPE.isUp())
			LCD.drawString(Boolean.toString(s.isPressed())+"  ",0,0);
	}
}

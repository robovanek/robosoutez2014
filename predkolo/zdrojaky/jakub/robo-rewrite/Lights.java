import lejos.nxt.*;
import lejos.util.Delay;

/**
 * US test
 */
public class Lights {
    public static void main(String[] args)
    {
        NXTMotor light = new NXTMotor(MotorPort.C);
        light.setPower(10);
        int power = 10;
        boolean up = true;
        LCD.clear();
        //UltrasonicSensor s = new UltrasonicSensor(SensorPort.S1);
        //TouchSensor minus = new TouchSensor(SensorPort.S3);
        //TouchSensor plus = new TouchSensor(SensorPort.S4);
        while(Button.ESCAPE.isUp())
        {
            /*
            int dist = s.getDistance();
            float range = s.getRange();
            LCD.drawString("DIST="+Integer.toString(dist)+"   ",0,0);
            LCD.drawString("RANGE="+Float.toString(range)+"   ",0,1);
            Delay.msDelay(200);
            */

            if(up)
            {
                if(power < 100)
                    power+=5;
                else
                    up=false;
            }
            else
            {
                if(power > 0)
                    power-=5;
                else
                    up=true;
            }
            light.setPower(power);
            Delay.msDelay(10);

            /*
            if(up)
            {
                light.setPower(100);
                up=false;
            }
            else
            {
                light.setPower(0);
                up=true;
            }
            Delay.msDelay(200);
            */
        }
        while(power<0)
        {
            power--;
            light.setPower(power);
            Delay.msDelay(10);
        }
    }
}

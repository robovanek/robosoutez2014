import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

/**
 * US test
 */
public class UStest {
    public static void main(String[] args)
    {
        UltrasonicSensor s = new UltrasonicSensor(SensorPort.S1);
        LCD.clear();
        while(Button.ESCAPE.isUp())
        {
            int dist = s.getDistance();
            LCD.drawString(dist+"  ",0,0);
        }
    }
}

import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.util.Delay;

/**
 * Light blinker.
 */
public class Blikac extends Thread {
	public static final int LWM = 0;
	public static final int HWM = 100;
	public static final int MOVE = 5;
	public static final int WAIT = 5;
	public boolean up=true;
	public int power = LWM;

	NXTMotor m;
	Thread parent;
	public Blikac(Thread origin)
	{
		m = new NXTMotor(MotorPort.C);
		m.setPower(power);
		parent = origin;
	}
	@Override
	public void run() {
		while (parent.isAlive()) {
			if (up) {
				if (power < HWM)
					m.setPower(power += MOVE);
				else
					up = false;
			} else {
				if (power > LWM)
					m.setPower(power -= MOVE);
				else
					up = true;
			}
			Delay.msDelay(WAIT);
		}
	}

}

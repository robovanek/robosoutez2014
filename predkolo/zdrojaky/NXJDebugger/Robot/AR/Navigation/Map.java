package AR.Navigation;

import AR.Utils.Common;

import java.io.*;
import java.nio.file.*;
import java.util.*;

/**
 * Virtuální mapa
 * Pozn. blok = pole
 * TODO vymyslet lepší navigační algoritmus
 * @author Jakub Vaněk
 */
public class Map {
	// PROMĚNNÉ A KONSTANTY
	/**
	 * X-ová velikost mapy (svislá)
	 */
	public final int sizeX = 11;
	/**
	 * Y-ová velikost mapy (vodorovná)
	 */
	public final int sizeY = 8;
	/**
	 * Pozice středu mapy (startu)
	 */
	public final Pos center = new Pos(5, 4);
	/**
	 * Interní reprezentace
	 */
	private int[][] map;
	/**
	 * Současná orientace robota; start je nahoru
	 */
	public int curOrient = MapOrientation.North;
	/**
	 * Současná pozice robota; robot startuje na startu ;)
	 */
	public Pos curPos = new Pos(center.x, center.y);
	/**
	 * Celkový počet překážek, nepatří sem start a okraje
	 */
	public final int maxWalls = 11;
	/**
	 * Současný počet nalezených zdí
	 */
	public int curWalls = 0;


	// FUNKCE
	/**
	 * Konstruktor
	 */
	public Map() {
		// Vytvořit mapu
		map = new int[sizeX][sizeY];
		// Naplnit mapu neznámými bloky
		for (int[] part : map)
			Arrays.fill(part, TileType.Unknown);
		// Nakreslit okraje
		for (int x = 0; x < sizeX; x++) { // vodorovně
			set(x, 0,         TileType.Wall); // nahoře
			set(x, sizeY - 1, TileType.Wall); // dole
		}
		for (int y = 1; y < sizeY - 1; y++) { // svisle
			set(0,         y, TileType.Wall); // vlevo
			set(sizeX - 1, y, TileType.Wall); // vpravo
		}
		// start a bloky kolem cíle jsou také překážky, ty se však do součtu nepočítají
		set(center.x - 1, center.y, TileType.Wall);
		set(center.x,     center.y, TileType.Wall);
		set(center.x + 1, center.y, TileType.Wall);
	}

	/**
	 * Získá pole z mapy
	 * @param p pozice
	 * @return pole
	 */
	public int get(Pos p)
	{
		return get(p.x,p.y);
	}

	/**
	 * Získá pole z mapy
	 * @param x souřadnice X
	 * @param y souřadnice Y
	 * @return pole
	 */
	public int get(int x, int y)
	{
		return map[x][y];
	}

	/**
	 * Nastaví pole v mapě
	 * @param x souřadnice X
	 * @param y souřadnice Y
	 * @param value hodnota
	 */
	public void set(int x, int y, int value)
	{
		map[x][y] = value;
	}

	/**
	 * Načte mapu ze souboru na kostce
	 * @param path Cesta k souboru
	 * @throws Exception Případná IO chyba
	 */
	public void load(String path) throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream fstr = classLoader.getResourceAsStream(path);
		char[] all = new char[sizeX*sizeY]; // seznam všech bloků
		// dokud je místo a dokud máme data ze souboru
		for(int index = 0; fstr.available() > 0 && index < all.length; index++)
		{
			int result=fstr.read(); // přečti znak
			if(result!=-1) // pokud se to povedlo
				all[index]=(char)result; // ulož výsledek
			else // pokud se to nepovedlo
				break; // skonči smyčku
		}
		fstr.close(); // uzavři proud dat
		int index = 0; // opět iterace
		for (int y = 0; y < sizeY; y++) { // procházej po sloupcích
			for (int x = 0; x < sizeX; x++) { // v každém sloupci procházej po řádcích
				switch (all[index]) { // větvení na základě znaku
					case 'X': // blok je zeď
						set(x,y,TileType.Wall);
						break;
					case '_': // blok je volné místo
						set(x,y,TileType.LightOn);
						break;
					default: // blok je neznámý, předpokládá se volné místo
						set(x,y,TileType.LightOn);
						break;
				}
				index++; // posuň se
			}
		}
		// zakáže prohledávání mapy
		curWalls = maxWalls; // MAPA MUSÍ BÝT SPRÁVNĚ ZADÁNA, FIXME ROBOT JEDE NASLEPO
	}

	/**
	 * Zatočí v mapě
	 * @param toRight
	 *            Jestli se má točit doprava
	 * @param count
	 *            Kolik 90° otočení se má udělat
	 */
	public void turn(boolean toRight, int count) {
		if (!toRight) // pokud netočíme doprava
			count *= -1; // udělej počet záporný
		curOrient = MapOrientation.edit(curOrient, count); // otoč směr
	}

	/**
	 * Jede přímo dopředu/dozadu o daný počet polí; započítává projetá pole
	 * @param count
	 *            Count of tiles to travel
	 * @param backward
	 *            Whether to go backward or not
	 */
	public void travel(int count, boolean backward) {
		// orientace
		int myOrient = curOrient;
		// zde byl bug, pokud jedeme dozadu, "otočit se"
		if (backward)
			myOrient = MapOrientation.edit(curOrient, 2);
		switch (myOrient) { // větvení ze směru
		case MapOrientation.North:
			for (int y = curPos.y, i = 0; i <= count; i++, y--) // úbytek Y
				set(curPos.x,y,TileType.LightOff); // nastav celou cestu na LightOff
			break;
		case MapOrientation.East:
			for (int x = curPos.x, i = 0; i <= count; i++, x++) // příbytek X
				set(x,curPos.y,TileType.LightOff); // nastav celou cestu na LightOff
			break;
		case MapOrientation.South:
			for (int y = curPos.y, i = 0; i <= count; i++, y++) // příbytek Y
				set(curPos.x,y,TileType.LightOff); // nastav celou cestu na LightOff
			break;
		case MapOrientation.West:
			for (int x = curPos.x, i = 0; i <= count; i++, x--) // úbytek X
				set(x,curPos.y,TileType.LightOff); // nastav celou cestu na LightOff
			break;
		}
		curPos = Pos.modify(curPos, myOrient, count); // upraví samotnou pozici robota
	}

	/**
	 * Zapíše do mapy počet volných polí
	 * @param freeTiles počet volných polí
	 * @param USorient směr ultrazvukového senzoru
	 * @param wall jestli senzor uviděl zeď
	 */
	public void writeDetection(int freeTiles, int USorient, boolean wall) {
		int abs = MapOrientation.sensorEdit(curOrient, USorient); // otočení ultrazvukového senzoru vzhledem k mapě
		try {
			switch (abs) { // větvení ze směru
				case MapOrientation.North: // nahoru
					for (int y = curPos.y, i = 0; i <= freeTiles + 1 && y >= 0; i++, y--) {
						write_replace(i, freeTiles, curPos.x, y, wall); // přepsat pole
					}
					break;

				case MapOrientation.East: // doprava
					for (int x = curPos.x, i = 0; i <= freeTiles + 1 && x < sizeX; i++, x++) {
						write_replace(i, freeTiles, x, curPos.y, wall); // přepsat pole
					}
					break;

				case MapOrientation.South: // dolů
					for (int y = curPos.y, i = 0; i <= freeTiles + 1 && y < sizeY; i++, y++) {
						write_replace(i, freeTiles, curPos.x, y, wall); // přepsat pole
					}
					break;

				case MapOrientation.West: // doleva
					for (int x = curPos.x, i = 0; i <= freeTiles + 1 && x >= 0; i++, x--) {
						write_replace(i, freeTiles, x, curPos.y, wall); // přepsat pole
					}
					break;
			}
		} catch (ArrayIndexOutOfBoundsException e) // To by se nemělo dál stávat
		{                                        // stávalo se to kvůli značkování až mimo mapu
			System.err.println("writeDetection(): array index overflow!");
		}
		// pokud byly objeveny všechny zdi, nahraď všechny neznámé svítícím
		// světlem
		if (curWalls >= maxWalls)
			for (int x = 0; x < sizeX; x++)
				for (int y = 0; y < sizeY; y++)
					if (get(x, y) == TileType.Unknown)
						set(x, y, TileType.LightOn);
	}

	private void write_replace(int i, int freeTiles, int x, int y, boolean wall) {
		if (i < freeTiles + 1) { // nastaví rozsvícené světlo
			if (get(x, y) == TileType.Unknown)
				set(x, y, TileType.LightOn);
		} else { // na konec nastaví zeď
			if (get(x, y) == TileType.Unknown && wall) {
				set(x, y, TileType.Wall);
				curWalls++;
			}
		}
	}

	/**
	 * Získá číslo kolik určitých polí je přede zdí
	 * Nelze vyhledávat zeď
	 *
	 * @param right
	 *            Jestli se má skenovat vpravo
	 * @param tile
	 *            Blok ke hledání
	 * @return Počet bloků
	 */
	public int beforeWall(boolean right, int tile) {
		int retVal = 0; // návratová hodnota
		
		int orient = right ? MapOrientation.edit(curOrient, 1) : MapOrientation.edit(curOrient, -1); // otočení doleva nebo doprava
		
		switch (orient) { // větvení ze směru
		case MapOrientation.North: // nahoru
			for (int i = 1, y = curPos.y - 1; y >= 0; i++, y--) { // dokud nevyjedeme z bludiště, koukej dál
				if (get(curPos.x,y) == TileType.Wall) // pokud je tento blok zeď
					break; // skonči
                if (get(curPos.x,y) == tile) // pokud je tento blok ten hledaný
				    retVal++; // přičti jej
			}
			break;
		case MapOrientation.East: // doprava
			for (int i = 1, x = curPos.x + 1; x < sizeX; i++, x++) { // dokud nevyjedeme z bludiště, koukej dál
				if (get(x,curPos.y) == TileType.Wall) // pokud je tento blok zeď
					break; // skonči
                if(get(x,curPos.y) == tile) // pokud je tento blok ten hledaný
                    retVal++; // přičti jej
			}

			break;
		case MapOrientation.South: // dolů
			for (int i = 1, y = curPos.y + 1; y < sizeY; i++, y++) { // dokud nevyjedeme z bludiště, koukej dál
				if (get(curPos.x,y) == TileType.Wall) // pokud je tento blok zeď
					break; // skonči
                if(get(curPos.x,y) == tile) // pokud je tento blok ten hledaný
                    retVal++; // přičti jej
			}

			break;
		case MapOrientation.West: // doprava
			for (int i = 1, x = curPos.x - 1; x >= 0; i++, x--) { // dokud nevyjedeme z bludiště, koukej dál
				if (get(x,curPos.y) == TileType.Wall) // pokud je tento blok zeď
					break; // skonči
                if(get(x,curPos.y) == tile) // pokud je tento blok ten hledaný
                    retVal++; // přičti jej
			}

			break;
		}
		return retVal; // vrať součet
	}
	
	/**
	 * Zjistí jestli se daný blok nacházi nalevo od robota
	 * @param tile Blok k ověření
	 * @return Jestli zde tento blok je
	 */
	public boolean isLeft(int tile){
		int orient = MapOrientation.edit(curOrient, -1); // otočení doleva
        return isOn(tile, orient); // vrácení toho, jestli je v tomto směru blok
	}

    /**
     * Zjistí jestli se daný blok nacházi napravo od robota
     * @param tile Blok k ověření
     * @return Jestli zde tento blok je
     */
    public boolean isRight(int tile){
        int orient = MapOrientation.edit(curOrient, 1); // otočení doprava
        return isOn(tile, orient); // vrácení toho, jestli je v tomto směru blok
    }

    /**
     * Zjistí jestli je v daném směru před námi daný blok
     * @param tile Požadovaný blok
     * @param orient Směr robota
     * @return Jestli zde je tento blok
     */
    public boolean isOn(int tile, int orient) {
        switch (orient) { // větvení ze směru
        case MapOrientation.North: // nahoru
            if (get(curPos.x,curPos.y-1) == tile) { // test přítomnosti bloku
                return true;
            }
            break;
        case MapOrientation.East: // doprava
            if (get(curPos.x+1,curPos.y) == tile) { // test přítomnosti bloku
                return true;
            }
            break;
        case MapOrientation.South: // dolů
            if (get(curPos.x,curPos.y+1) == tile) { // test přítomnosti bloku
                return true;
            }
            break;
        case MapOrientation.West: // doleva
            if (get(curPos.x-1,curPos.y) == tile) { // test přítomnosti bloku
                return true;
            }
            break;
        }
        return false;
    }

	
	/**
	 * Najde cestu k nejbližšímu požadovanému bloku pomocí šířkového prohledávání.
	 * TODO používat něco lepšího
	 * @param start
	 *            Startovní pozice
	 * @param request
	 *            Požadovaný blok
	 * @param reject
	 *            Překážky přes které nelze jet
	 * @return Nejkratší cesta k požadovanému bloku
	 * @deprecated Dělá to problémy, je potřeba si udělat svůj algoritmus
	 */
	public Path BFSearch(Pos start, int request, int[] reject) {
		// INICIALIZACE
		java.util.Map<Pos, Pos> parent = new HashMap<>(); // pole potomek-rodič
		parent.put(start, Pos.none); // start je kořen

		List<Pos> frontier = new ArrayList<>(); // otevřené uzly
		frontier.add(start); // start je první
		Pos nearest = Pos.none; // pozice nejbližšího bloku

		// PRŮCHOD GRAFEM (MAPOU)
		hledani: // označení vnější smyčky
		while (frontier.size() != 0) // dokud máme co procházet
		{
			List<Pos> next = new ArrayList<>(); // budoucí průchod
			for (Pos u : frontier) // projdeme otevřené uzly
			{
				Pos[] adjacent = Pos.adjacent(u); // získáme okolí uzlu
				for (Pos v : adjacent) // projdeme okolí uzlu
				{
					if (!(parent.containsKey(v) || Common.aContainsO(get(v), reject)))
					// pokud máme fresh uzel a ten není zábrana
					{
						parent.put(v, u); // Nastavit rodiče -> navštíven
						if (get(v) == request) { // Pokud jsme narazili na požadovaný blok
							nearest = v; // přiřadit
							break hledani; // TADÁ ODSUD
						}
						next.add(v); // Přidat do dalšího průchodu
					}
				}
			}
			frontier = next; // přeřazení, minulá budoucnoust = současnost
		} // TADÁ SEM

		// HLEDÁNÍ CESTY
		if (nearest == Pos.none) // Pokud žádný blok nebyl nalezen
			return null; // vrátíme nic

		Stack<Pos> path = new Stack<>(); // LIFO, zde bude pořadí cíl->start
		Pos explorPos = nearest; // první bod
		while (explorPos != Pos.none) { // procházet od potomků k rodičům, dokud nejsme na startu
			path.push(explorPos); // Přidat uzel
			explorPos = parent.get(explorPos); // Posunout se dopředu
		}
		Queue<Pos> retVal = new LinkedList<>(); // FIFO
		while (path.size() != 0) // postupné převrácení, nové pořadí je start->cíl
			retVal.add(path.pop());
		return new Path(retVal); // Vrátit cestu

	}

	/**
	 * Zjistí jestli je nalevo od robota volno
	 * @return jestli nalevo od robota není zeď
	 */
	public boolean freeLeft()
	{
		return !isLeft(TileType.Wall); // If in map is free
	}

	/**
	 * Zjistí jestli je napravo od robota volno
	 * @return jestli napravo od robota není zeď
	 */
	public boolean freeRight()
	{
		return !isRight(TileType.Wall);
	}

	/**
	 * Zjistí jestli je na stranách robota volno
	 * @return jestli nalevo i napravo od robota není zeď
	 */
	public boolean sidesFree() {
		return freeLeft() && freeRight();
	}

}

package nxjsim.GUI;
import javafx.application.Platform;
import lejos.nxt.NXTRegulatedMotor;
import nxjsim.core.EventListener;

import java.util.Random;

public class SimEndpoint implements EventListener {
	public Control frontend;
	public Random random;
	public SimEndpoint(Control frontend)
	{
		this.frontend = frontend;
		random = new Random();
		//timer = new Timer();
		//timer.schedule(new TimerTask() {
		//	@Override
		//	public void run() {
		//		Platform.runLater(frontend::draw);
		//	}
		//},100,100);
	}
	@Override
	public void MotorModified(NXTRegulatedMotor motor, NXTRegulatedMotor.MotorEvent e, int value)
	{
		Thread d = new Thread(()-> Platform.runLater(frontend::draw));
		d.start();
		lejos.util.Delay.msDelay(200);
	} // nothing

	@Override
	public int getUSvalue() {
		return random.nextInt(5)*28; // RANDOM :D
	}

	@Override
	public boolean getTSpress() {
		return true;
	}

	@Override
	public int getGyroDirection() {
		int num = random.nextInt(360);
		return random.nextInt(50)%2==0 ? num: -num;
	}

	@Override
	public void buttonPress() {
		Platform.runLater(frontend::pressButton);

	}
}

package lejos.nxt;
import nxjsim.core.EventListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuba on 2.11.14.
 */
public class UltrasonicSensor {
    public static List<EventListener> listeners = new ArrayList<>();
    public static void addListener(EventListener e)
    {
        listeners.add(e);
    }
    public UltrasonicSensor(SensorPort a)
    {}
    public int getDistance()
    {
        return listeners.get(0).getUSvalue();
    }
}

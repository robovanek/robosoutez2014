package lejos.nxt;

import nxjsim.core.EventListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Button class.
 * @author Jakub Vaněk
 */
public class Button {
	public static List<EventListener> listeners = new ArrayList<EventListener>();

	public static void addListener(EventListener e) {
		listeners.add(e);
	}

	public static int waitForAnyPress() {
		listeners.get(0).buttonPress();
		return 0;
	}

	public static final int ID_ESCAPE = 0xD;
	public static final Button ESCAPE = new Button();
}
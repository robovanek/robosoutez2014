package lejos.nxt;

/**
 * Created by kuba on 9.2.15.
 */
public class MotorPort {
	private MotorPort(){}
	public static final MotorPort A = new MotorPort();
	public static final MotorPort B = new MotorPort();
	public static final MotorPort C = new MotorPort();
}

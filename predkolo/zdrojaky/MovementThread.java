import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import java.lang.Math;

/**
 * Thread handling all the movement of the robot. Left motor is in the A port and right one in the B port.
 * @author Filip Smola
 *
 */
public class MovementThread implements Runnable {
	/**
	 * The power the motors are set to for regular forward movement
	 */
	private static final int FORWARD_PWR = 180;
	/**
	 * turning constant
	 */
	private double turn_constant;
	/**
	 * Straight constant
	 */
	private double straigth_constant;
	/**
	 * The left motor
	 */
	private static NXTRegulatedMotor left;
	/**
	 * The right motor
	 */
	private static NXTRegulatedMotor right;
	
	/**
	 * Assigns motors to their variables
	 */
	public MovementThread(){
		left = Motor.A;
		right = Motor.B;
	}
	
	@Override
	public void run() {
		//TO-DO
	}
	/**
	 * Init constants
	 * @param wheelDiameter Wheel diameter
	 * @param trackWidth Track width
	 * @param wheelBase Wheelbase length
	 * @author Jakub Vaněk
	 */
	public void Init(int wheelDiameter, int trackWidth, int wheelBase){
		turn_constant = Math.sqrt(trackWidth*trackWidth+wheelBase*wheelBase)/wheelDiameter;
		straigth_constant = wheelDiameter*Math.PI;
	}
	/**
	 * Get turning angle
	 * @param degree Requested angle
	 * @return Angle for motors
	 * @author Jakub Vaněk
	 */
	private int turn_degree(int degree){
		return (int) (turn_constant * degree);
	}
	/**
	 * Get straight angle
	 * @param distance Distance in cm
	 * @return Angle for motors
	 * @author Jakub Vaněk
	 */
	private int straight_degree(int distance){
		return (int) (distance/straigth_constant*360);
	}
	/**
	 * Starts the turning of the robot by setting the motors to opposite speeds (one fifth of the forward speed).
	 * @param goRight Whether to go right (true) or go left (false)
	 */
	private void startTurning(boolean goRight){
		if(goRight){
			left.setSpeed(FORWARD_PWR / 5);
			right.setSpeed(-FORWARD_PWR / 5);
		}else{
			left.setSpeed(-FORWARD_PWR / 5);
			right.setSpeed(FORWARD_PWR / 5);
		}
	}
	/**
	 * turn robot by specified degree
	 * TODO accuracy enchancement by gyroscope on S2, could be PID regulator
	 * @param goRight Whether to go right (true) or go left (false)
	 * @param degree How many degree to turn
	 * @param reverseturn Whether to switch turning wheels spin
	 * @author Jakub Vaněk
	 */
	private void turn(boolean goRight, int degree, boolean reverseturn){
		int rotation = turn_degree(degree);
		left.setSpeed(FORWARD_PWR / 5);
		right.setSpeed(FORWARD_PWR / 5);
		if(!reverseturn){	
			if(goRight){
				left.rotate(rotation);
				right.rotate(-rotation);
			}else{
				left.rotate(-rotation);
				right.rotate(rotation);
			}
		}
		else{
			if(goRight){
				left.rotate(-rotation);
				right.rotate(rotation);
			}else{
				left.rotate(rotation);
				right.rotate(-rotation);
			}
		}
		left.setSpeed(FORWARD_PWR);
		right.setSpeed(FORWARD_PWR);
	}
	/**
	 * Travel specified distance
	 * @param wheelDiameter Diameter of wheel
	 * @param centimeters Travel distance in cm
	 * @param backward Whether to go backward
	 * @author Jakub Vaněk
	 */
	private void travelDistance(int wheelDiameter, int centimeters, boolean backward){
		int rotation = straight_degree(centimeters);
		if(backward)
			rotation=rotation*-1;
		right.rotate(rotation);
		left.rotate(rotation);
	}
	/**
	 * Travel indefinite distance
	 * @param backward Whether to go backward
	 * @author Jakub Vaněk
	 */
	private void travel(boolean backward)
	{
		if(!backward){
			right.forward();
			left.forward();
		}else{
			right.backward();
			left.backward();
		}
	}

}
